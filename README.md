# Cognition and the Stability of Evolving Complex Morphology: An Agent-based Model

## EvoLang 2022

Welcome to the landing page for our EvoLang 2022 paper. From here you can reach:

- Our [model code](https://gitlab.com/stephenfmann/paradigm)
- A [wiki](https://stephen-mann.gitbook.io/paradigm-agent-based/) for the code
- A [live online simulation demonstration](https://smg-agent-based.appspot.com/)

This repository also contains (see above):

- Our poster
- A short 3 minute video
- The full paper

Very best,

Erich Round, Stephen Francis Mann, Sacha Beniamine, Emily Lindsay-Smith, Louise Esher and Matt Spike
